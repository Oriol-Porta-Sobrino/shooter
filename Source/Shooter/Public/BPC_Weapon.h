#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BPC_Weapon.generated.h"

class AFpsCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER_API UBPC_Weapon : public UActorComponent
{
	GENERATED_BODY()

public:	
	UBPC_Weapon();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void AttachWeapon(AFpsCharacter* apFpsCharacter);

	UFUNCTION(BlueprintCallable)
	void OnDropCallBack(AFpsCharacter* apFpsCharacter);

	UFUNCTION(BlueprintCallable)
	void OnColdDownCallBack();
	
	UFUNCTION(BlueprintCallable)
	void OnFireCallback();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UBillboardComponent* mpMuzzleOffset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float mShootRange {5000.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float mDamage {50.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	AFpsCharacter* mpOwnerCharacter;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FTimerHandle mCoolDownHandler;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool mIsCooledDown {false};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float mColdDownTime {1.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float mRayCastTime {0.5f};
		
};
