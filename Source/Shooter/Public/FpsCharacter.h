// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputAction.h"
#include "Utils.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Character.h"
#include "FpsCharacter.generated.h"

class UBPC_Weapon;
class UInputMappingContext;
class UInputConfigData;
class UCharacterMovementComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnClick);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDropOnQ, AFpsCharacter*, Character);

UCLASS()
class SHOOTER_API AFpsCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFpsCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UStaticMeshComponent* GetWeapon() const {return mpWeaponPoint;}
	UCameraComponent* GetCamera() const {return mpCamera;}

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* mpWeaponPoint;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputMappingContext* mpInputMapping;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputConfigData* mpInputData;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int mMoveSpeed {2U};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float mCurrentHealth {100.f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float mMaxHealth {100.f};

	UPROPERTY(BlueprintAssignable)
		FOnClick evOnClick;

	UPROPERTY(BlueprintAssignable)
		FDropOnQ evDropOnQ;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		UBPC_Weapon* mpWeaponEquipped;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	void OnClickCallback(const FInputActionValue& aValue);

	void OnDropWeapon(const FInputActionValue& aValue);

	void LookCallback(const FInputActionValue& aValue);
	
	void MoveCallback(const FInputActionValue& aValue);

	UPROPERTY(EditDefaultsOnly)
		UCameraComponent* mpCamera;

};
