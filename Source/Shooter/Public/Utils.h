#pragma once

#define LogD(text) UE_LOG(LogTemp, Warning, TEXT(text))
#define ScreenD(text) if(GEngine != nullptr) {GEngine->AddOnScreenDebugMessage(-1, 2.0, FColor::Black, text);}
#define Format1(x, y) FString::Printf(TEXT(x),y)