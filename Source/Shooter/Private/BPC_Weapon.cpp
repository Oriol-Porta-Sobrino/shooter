#include "BPC_Weapon.h"

#include "FpsCharacter.h"
#include "Utils.h"
#include "Components/BillboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

UBPC_Weapon::UBPC_Weapon()
{
	PrimaryComponentTick.bCanEverTick = true;
	mpMuzzleOffset = CreateDefaultSubobject<UBillboardComponent>(TEXT("ShootPoint"));
}

void UBPC_Weapon::AttachWeapon(AFpsCharacter* apFpsCharacter)
{
	mpOwnerCharacter = apFpsCharacter;
	if (mpOwnerCharacter != nullptr && mpOwnerCharacter->mpWeaponEquipped == nullptr)
	{
		mpOwnerCharacter->mpWeaponEquipped = this;
		FAttachmentTransformRules AttachRules {EAttachmentRule::SnapToTarget, true};
		GetOwner()->AttachToComponent(mpOwnerCharacter->GetWeapon(), AttachRules, FName(TEXT("WeaponPointSocket")));
		mpOwnerCharacter->evOnClick.AddDynamic(this, &UBPC_Weapon::OnFireCallback);
		mpOwnerCharacter->evDropOnQ.AddDynamic(this, &UBPC_Weapon::OnDropCallBack);
	}
}

void UBPC_Weapon::OnDropCallBack(AFpsCharacter* apFpsCharacter)
{
	mpOwnerCharacter = apFpsCharacter;
	if (mpOwnerCharacter != nullptr && mpOwnerCharacter->mpWeaponEquipped != nullptr)
	{
		mpOwnerCharacter->mpWeaponEquipped = nullptr;
		FVector NewLocation = mpOwnerCharacter->GetActorLocation();
		GetOwner()->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		NewLocation.Z = 100.0f; // Cambia el valor según tu necesidad
		GetOwner()->SetActorLocation(NewLocation);
		mpOwnerCharacter->evOnClick.RemoveDynamic(this, &UBPC_Weapon::OnFireCallback);
		mpOwnerCharacter->evDropOnQ.RemoveDynamic(this, &UBPC_Weapon::OnDropCallBack);
	}
}

void UBPC_Weapon::OnColdDownCallBack()
{
	mIsCooledDown = false;
}

//Throws a raycast to show if something is hit and what has been hit
void UBPC_Weapon::OnFireCallback()
{
	if (mpOwnerCharacter == nullptr) return;
	const UWorld* pWorld {GetWorld()};
	if (pWorld == nullptr) return;
	if (mIsCooledDown) return; 

	GetWorld()->GetTimerManager().SetTimer(mCoolDownHandler, this, &UBPC_Weapon::OnColdDownCallBack, mColdDownTime, false);
	mIsCooledDown = true;
	const UCameraComponent* PlayerCamera {mpOwnerCharacter->GetCamera()};
	const FRotator CameraRotation {PlayerCamera->GetComponentRotation()};
	const FVector StartLocation {GetOwner()->GetActorLocation() + CameraRotation.RotateVector(mpMuzzleOffset->GetComponentLocation())};
	const FVector EndLocation {StartLocation + UKismetMathLibrary::GetForwardVector(PlayerCamera->GetComponentRotation()) * mShootRange};

	FCollisionQueryParams QueryParams {};
	QueryParams.AddIgnoredActor(mpOwnerCharacter);
	FHitResult HitResult {};
	pWorld->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Camera, QueryParams);
	DrawDebugLine(pWorld, StartLocation, EndLocation, HitResult.bBlockingHit ? FColor::Blue : FColor::Red, false, mRayCastTime, 0, 10.f);
	if (HitResult.bBlockingHit && IsValid(HitResult.GetActor()))
	{
		ScreenD(Format1("%s", *HitResult.GetActor()->GetName()));
		UGameplayStatics::ApplyDamage(HitResult.GetActor(), mDamage, mpOwnerCharacter->GetController(), GetOwner(), {});
	}
}

void UBPC_Weapon::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UBPC_Weapon::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

