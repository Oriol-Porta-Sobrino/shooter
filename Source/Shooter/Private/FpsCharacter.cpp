// Fill out your copyright notice in the Description page of Project Settings.


#include "..\Public\FpsCharacter.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputConfigData.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Shooter/Public/Utils.h"

// Sets default values
AFpsCharacter::AFpsCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mpWeaponPoint = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponPoint"));
	mpWeaponPoint->bCastDynamicShadow = false;
	mpWeaponPoint->CastShadow = false;

	mpCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCamera"));
	mpCamera->SetupAttachment(GetCapsuleComponent());
	mpCamera->bUsePawnControlRotation = true;

	UCharacterMovementComponent* mCharacterMovement {GetCharacterMovement()};
	mCharacterMovement->BrakingFriction = 10.f;
	mCharacterMovement->MaxAcceleration = 10000.f;
	mCharacterMovement->MaxWalkSpeed = 1000.f;
	mCharacterMovement->JumpZVelocity = 600.f;
	mCharacterMovement->AirControl = 1.f;
}

// Called when the game starts or when spawned
void AFpsCharacter::BeginPlay()
{
	Super::BeginPlay();
	mpWeaponPoint->AttachToComponent(GetMesh(), {EAttachmentRule::SnapToTarget, true}, TEXT("WeaponPointSocket"));
}

// Called every frame
void AFpsCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AFpsCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);

	APlayerController* PlayerController = Cast<APlayerController>(GetController());
	UEnhancedInputLocalPlayerSubsystem* EInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());
	EInputSubsystem->ClearAllMappings();
	EInputSubsystem->AddMappingContext(mpInputMapping, 0);

	UEnhancedInputComponent* EInputComponent = {Cast<UEnhancedInputComponent>(PlayerInputComponent)};
	EInputComponent->BindAction(mpInputData->InputMove, ETriggerEvent::Triggered, this, &AFpsCharacter::MoveCallback);
	EInputComponent->BindAction(mpInputData->InputLook, ETriggerEvent::Triggered, this, &AFpsCharacter::LookCallback);
	EInputComponent->BindAction(mpInputData->InputJump, ETriggerEvent::Triggered, this, &ACharacter::Jump);
	EInputComponent->BindAction(mpInputData->InputDropWeapon, ETriggerEvent::Triggered, this, &AFpsCharacter::OnDropWeapon);
	EInputComponent->BindAction(mpInputData->InputMouseClick, ETriggerEvent::Triggered, this, &AFpsCharacter::OnClickCallback);
}

void AFpsCharacter::MoveCallback(const FInputActionValue& aValue)
{
	if (IsValid(Controller))
	{
		const FVector2d MoveValue {aValue.Get<FVector2d>()};
		const FRotator MoveRotator {0, Controller->GetControlRotation().Yaw, 0};

		if (MoveValue.Y != 0)
		{
			const FVector Dir {MoveRotator.RotateVector(FVector::ForwardVector)};
			AddMovementInput(Dir, MoveValue.Y);
		}
		if (MoveValue.X != 0)
		{
			const FVector Dir {MoveRotator.RotateVector(FVector::RightVector)};
			AddMovementInput(Dir, MoveValue.X);
		}
	}
}

void AFpsCharacter::LookCallback(const FInputActionValue& aValue)
{
	if (IsValid(Controller))
	{
		const FVector2D LookValue {aValue.Get<FVector2d>()};
		if (LookValue.X != 0)
		{
			AddControllerYawInput(LookValue.X);
		}
		if (LookValue.Y != 0)
		{
			AddControllerPitchInput(LookValue.Y);
		}
	}
}

void AFpsCharacter::OnClickCallback(const FInputActionValue& aValue)
{
	if (IsValid(Controller))
	{
		evOnClick.Broadcast();
	}
}

void AFpsCharacter::OnDropWeapon(const FInputActionValue& aValue)
{
	if (IsValid(Controller))
	{
		evDropOnQ.Broadcast(this);
	}
}



