Risketos Bàsics

 - [X] Control de personatge amb base ACharacter (o amb AController).
 - [X] Capacitat de disparar, ja sigui amb Trace line (Raycast) o amb objectes físics en moviment (Projectile).
 - [X] Utilitzar delegats per fer alguna acció, per exemple el disparar.
 - [X] Més d’un tipus d'armes i/o bales.
 - [X] Ús de les UMacros per privatització, escritura i lectura de forma correcta.

Risketos Opcionals

 - [ ] Ha d’implementar una petita inteligencia artificial als enemics.
 - [ ] En cas de bales físiques, fer una pool.
 - [ ] Control de jugador avançat.
 - [X] Us d’Animation Blueprint i Blendtrees.
 - [ ] Shaders.
 - [X] Dropear arma.



 



